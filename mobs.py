#!/usr/bin/python3

import os, subprocess

try:
    from PIL import Image # I hate you
except ImportError:
    import cv2

pwd = os.getcwd()
f = open("ATTR.txt", "w")
bf1=[];bf2=[]

for it in os.listdir(pwd + "/ready-mobs"):
    if not it.endswith("png"):
        continue

    name = it.split(".")[0]
    path = pwd + "/ready-mobs/"

    try:
        mobid = int(name)
    except:
        print("ERROR: %s is not parseable" % it)
        continue

    print("%s" % it)

    # Get image size
    # This is not needed; ImageMagick fixes aspect ratio on its own
    try:
        img = Image.open(path+it)
        w = img.width
        h = img.height
    except:
        img = cv2.imread(path+it)
        h, w = img.shape[:2]

    # Type 1 convertion
    # Max: 420x600
    w5 = int(w); h5 = int(h)

    # We resize based on "best dimension"
    # Relies on Python 3 division behavior
    if (w / h) >= 1:
        # Width is the better one
        h5 += ((420-w) * h) / w # As float
        w5 = 420
    else:
        # Height is the better one
        w5 += ((600-h) * w) / h # As float
        h5 = 600

    # Forced sanitization
    w5 = min(w5, 420)
    h5 = min(h5, 600)

    # Convert
    subprocess.call("convert %s/%s -resize %dx%d %s/95%04d.webp" % (path, it, w5, h5, path, mobid), shell=True)

    # Prepare the "BOSS" dimension
    # 2200 x 1800
    w6 = int(w); h6 = int(h)
    # We resize based on "best dimension"
    # Relies on Python 3 division behavior
    if (w / h) >= 1:
        # Width is the better one
        h6 += ((2200-w) * h) / w # As float
        w6 = 2200
    else:
        # Height is the better one
        w6 += ((1800-h) * w) / h # As float
        h6 = 1800

    # Forced sanitization
    w6 = min(w6, 2200)
    h6 = min(h6, 1800)

    # Convert
    subprocess.call("convert %s/%s -resize %dx%d %s/96%04d.webp" % (path, it, w6, h6, path, mobid), shell=True)

    # Save
    bf1.append("\t95%04d NAME (AUTHOR)\t(CC BY SA)\t(SOURCE)\n" % (mobid))
    bf2.append("\t96%04d NAME (AUTHOR)\t(CC BY SA)\t(SOURCE)\n" % (mobid))

# Flush
for it in sorted(bf1):
    f.write(it)
f.write("\n")
for it in sorted(bf2):
    f.write(it)

f.close()
print("\n\n*************************")
subprocess.call("cat %s/ATTR.txt" % pwd, shell=True)

