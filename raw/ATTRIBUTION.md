For DMCA complaints please email dmca@tmw2.org and we will try to correct it
within 30 (thirty) days.

This special license file covers assets only, for code, you must refer to their
headers and to the COPYING file.

Notes: Works submitted by Jesusalva and under CC0, may also be used as if they
were under the Apache license without the need of prior consent.

----

GPLv2: Licensed under GNU General Public License version2 or later.
https://www.gnu.org/licenses/gpl-2.0.html
See also the GPL file.

GPLv3: Licensed under GNU General Public License version3 or later.
https://www.gnu.org/licenses/gpl-3.0.html
See also the COPYING

CC BY-SA 4.0: Licensed under Creative Commons Attribution-ShareAlike 4.0 Unported.
https://creativecommons.org/licenses/by-sa/4.0/
See also the CC BY-SA 4.0 file.

CC0 : Licensed under Creative Commons Zero
https://creativecommons.org/licenses/cc0
Also known as “Public Domain Dedication”

MIT : Licensed under the MIT License.
https://opensource.org/licenses/MIT
See also MIT file.

CC-BY : Either 3.0 or 4.0, depending if version was specified or not.
https://creativecommons.org/licenses/by/3.0/
https://creativecommons.org/licenses/by/4.0/

---------------------------------------------------------------------------------

Units at squared share the same license as their originals but contains edits.
Mobs with prefix 96 share the same license as their counterpart with prefix 95 but
are upscaled.

---------------------------------------------------------------------------------

This copyright notice must be kept intact for use, according to CC BY-SA
compatibility terms.
Where relevant, you must also include a link to https://tmw2.org in your credit.

DO NOT assume endorsement from the authors listed below!
The artworks may have been modified without notation about the edits in order to
work with this software, we strongly advise to look at their original sources
first! Some modifications may also be applied on-the-fly by the client.

Support the artists! Good Open Source art is hard to find.

---------------------------------------------------------------------------------
Useful links:
https://opengameart.org/	(Open Game Art / OGA)
https://arcmage.org/	(Arcmage - A libre CCG)
https://github.com/doofus-01	(Doofus 1/BfW Git repository)
https://www.peppercarrot.com/	(Pepper Carrot, webcomics)
https://davidrevoy.com/	(David Revoy personal blog)
https://www.indiedb.com/company/iron-thunder	(Iron Thunder, game dev)
https://serpentsoundstudios.com/	(Alexander Nakarada, music website)
https://wesnoth.org/	(The Battle for Wesnoth / BfW)
https://argentumage.com/ (Citadel - Argentum Age)
https://archive.org/details/Wootha_Public_Domain (Wootha Public Domain Release)
https://spark.adobe.com/page/ucLFuLOEKK9Uo/ (Wootha's Homepage)

---------------------------------------------------------------------------------
# Folder
	path/to/the/licensed/work.something (Author)	(License)	(Notes/Contributors/Authors/Co-Authors/Sources/etc.)

# BG
	castle (yd)	(CC0)	(OpenGameArt)
	Human City (Santiago Iborra)	(CC BY SA)	(Arcmage)
	wilderness (Uncle Mungen)    (CC BY) (Lemma Soft Forums)

# MOBS
	.:: THE EMPIRE ::.
	950000 Spearman (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950001 Foot Soldier (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950002 Squad Leader (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950003 Conscript (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950004 Mounted Eagle (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950005 Band of Brothers (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: THE BANDITS ::.
	9501XX
	******

	.:: THE ELVES ::.
	950200 Archer (Kathrin Polikeit)	(CC BY SA)	(Arcmage)
	950201 Elven Justice (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950202 Elf Male Caster (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950203 Elven Coat (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950204 Eternal Protection (BramastaAji)	(CC BY SA)	(Arcmage)
	950205 Guard (BramastaAji)	(CC BY SA)	(Arcmage)
	950206 New Things (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950207 Serenity (BramastaAji)	(CC BY SA)	(Arcmage)
	950208 Shapeshift (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950209 Protection (BramastaAji)	(CC BY SA)	(Arcmage)
	950210 Female Elf Caster (Kathrin Polikeit)	(CC BY SA)	(Arcmage)
	******

	.:: THE DARK HORDE ::.
	950300 Cyclops (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: DARKLANDERS ::.
	950400 Deadly Shock (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950401 Dark Mana Breather (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: THE REBELS ::.
	950500 Female Rebel 2 (Doofus-01)	(CC BY SA)	(Archaic Resources - BfW)
	******

	.:: THE LAND OF FIRE ::.
	950600 Drone (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)
	950601 Mech Scout (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)
	******

	.:: THE WIZARD ORDER ::.
	950700 Fire Wizard Female (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950701 Delfador (Emilien Rotival)	(CC BY SA)	(Wesnoth - HttT)
	950702 Dimensional Pocket (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: THE DWARVES ::.
	950800 Dwarf Warrior (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: UNUSED OR GENERIC ::.
	950900 Brightshield (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)
	950901 Jezzabell (Ironthunder)	(CC BY 4.0)	(OpenGameArt)
	950902 Cloak 2 (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)
	
# DIALOG
	Foot Soldier (Santiago Iborra)	(CC BY SA)	(Arcmage)
	Squad Leader (Santiago Iborra)	(CC BY SA)	(Arcmage)
	Cloaked Man (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)

# UNITS
	101001 Red Dragon Economist (David Revoy)	(CC BY)	(DavidRevoy.com)
	101002 Victor (David Revoy)	(CC BY)	(DavidRevoy.com)
	101003 Finding Balance (David Revoy)	(CC BY)	(DavidRevoy.com)
	101004 Speed Painting Test Kritas (David Revoy)	(CC BY)	(DavidRevoy.com)
	101005 Dulling Test Stylised Girl (David Revoy)	(CC BY)	(DavidRevoy.com/Edit)
	101006 Happy New Year 2008 (David Revoy)	(CC BY)	(DavidRevoy.com/Edit)
	101007 If Uluk the Swift (Lisanguyen)	(CC BY SA)	(Argentum)
	101008 Anthem of Battle (StephenWang)	(CC BY SA)	(Argentum/Edit)
	101009 Cleric of the Vanguard (Lisanguyen)	(CC BY SA)	(Argentum/Edit)
	101010 Frost Blast (Enjinn)	(CC BY SA)	(Argentum/Edit)
	101011 Gezzix Lord of Death (Lisanguyen)	(CC BY SA)	(Argentum/Edit)
	101012 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101013 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101014 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101015 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101016 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101017 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101018 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101019 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101020 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101021 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101022 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101023 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101024 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101025 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101026 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101027 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101028 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101029 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101030 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	101031 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)

	102001 Krita Speed Character Concept (David Revoy)	(CC BY)	(DavidRevoy.com)
	102002 Coriander, Concept Art (David Revoy)	(CC BY)	(DavidRevoy.com)
	102003 Krita Color Demo Videos (David Revoy)	(CC BY)	(DavidRevoy.com)
	102004 Gooseberry Dragon Sheep Design (David Revoy)	(CC BY)	(DavidRevoy.com)
	102005 Electron Donor (David Revoy)	(CC BY)	(DavidRevoy.com)
	102006 Graphics Testing (David Revoy)	(CC BY)	(DavidRevoy.com/Edit)
	102007 Eji the Summoner (Lisanguyen)	(CC BY SA)	(Argentum)
	102008 Librarian (David Revoy)	(CC BY SA)	(DavidRevoy.com/Edited)
	102009 Rihns Anointed (Luciana Nascimento)	(CC BY SA)	(Argentum/Edit)
	102010 Shichimi in Combat Mode (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	102011 Winter (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	102012 Pepper (AUTHOR)	(CC BY SA)	(DavidRevoy.com)
	102013 Inquisitor (Luciana Nascimento)	(CC BY SA)	(Argentum/Edit)
	102014 Fire Elemental (Lisanguyen)	(CC BY SA)	(Argentum/Edit)
	102015 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102016 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102017 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102018 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102019 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102020 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102021 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102022 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102023 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102024 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102025 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102026 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102027 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102028 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102029 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102030 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102031 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102032 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102033 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102034 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102035 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102036 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	102037 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)

	103001 Flight of Spring (David Revoy)	(CC BY)	(DavidRevoy.com)
	103002 Witch of Magmah (David Revoy)	(CC BY)	(DavidRevoy.com)
	103003 03 Character Design (David Revoy)	(CC BY)	(DavidRevoy.com)
	103004 Christmas Card 2010 (David Revoy)	(CC BY)	(DavidRevoy.com/Edited)
	103005 Red Hair Elf Girl (David Revoy)	(CC BY)	(DavidRevoy.com/Edited)
	103006 Chaos & Evolutions (David Revoy)	(CC BY)	(DavidRevoy.com/Edited)
	103007 Eager Swordsman (Lisanguyen)	(CC BY SA)	(Argentum)
	103008 Acolyte (Lucy/Luciana Nascimento)	(CC BY SA)	(Argentum)
	103009 Knife in the Dark (Luciana Nascimento)	(CC BY SA)	(Argentum/Edit)
	103010 Spell Warden (Lisanguyen)	(CC BY SA)	(Argentum/Edit)
	103011 Sul Dahl Devout (Lisanguyen)	(CC BY SA)	(Argentum/Edit)
	103012 Loyal Guard (Lisanguyen)	(CC BY SA)	(Argentum/Edit)
	103013 Ilz Apprentice (Lord Bob)	(CC BY SA)	(Argentum/Edit)
	103014 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103015 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103016 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103017 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103018 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103019 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103020 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103021 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103022 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103023 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103024 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103025 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103026 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103027 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103028 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103029 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103030 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103031 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103032 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103033 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103034 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103035 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103036 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103037 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103038 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103039 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103040 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	103041 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)

	104001 Volcano Monster (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	104002 Dawn Final Hi (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	104003 Timelapse Lezard Revoy (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	104004 Winter Fairies (David Revoy)	(CC BY)	(DavidRevoy.com)
	104005 Episode 33 WIP Grayscale (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	104006 Saffron Steampunk Clothes (David Revoy)	(CC BY)	(DavidRevoy.com/Edit)
	104007 Deephelm Explorer (Stephenwang)	(CC BY SA)	(Argentum)
	104008 Devout Priest (Lord Bob)	(CC BY SA)	(Argentum)
	104009 Endless Studies (Luciana Nascimento)	(CC BY SA)	(Argentum)
	104010 Goodess (Luciana Nascimento)	(CC BY SA)	(Argentum)
	104011 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104012 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104013 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104014 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104015 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104016 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104017 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104018 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104019 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104020 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104021 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104022 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104023 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104024 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104025 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104026 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104027 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104028 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	104029 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)

	105001 Nourished Flower (Santiago Iborra)	(CC BY SA)	(Arcmage)
	105002 Powerful (David Revoy)	(CC BY)	(DavidRevoy.com)
	105003 Owl Princess (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	105004 HEROART (Mohammed Agbadi/Sine Nomine Publishing)	(CC0)	(OGA/Edited)
	105005 Janus the Great (Stephenwang)	(CC BY SA)	(Argentum/Edit)
	105006 Oldric Lord of the Hold (Wendyyoon)	(CC BY SA)	(Argentum/Edit)
	105007 Rainbow Mage (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	105008 Thunderer (Lord Bob)	(CC BY SA)	(Argentum/Edit)
	105009 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105010 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105010 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105011 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105012 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105013 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105014 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105015 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105016 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)
	105017 AI GENERATED ART (Ophoria)	(CC BY SA)	(MidJourney)

	900001 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)
	900002 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)
	900003 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)
	900004 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)
	900005 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)

	900021 Treasure (Ironthunder)	(CCBY)	(bg by kindland[CC0]/Edited)

	900011 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)
	900012 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)
	900013 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)
	900014 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)
	900015 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)

	900031 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)

# SUMMONS
	0   Worker 03 (Stéphane Richard)	(PD)	(Wootha/Edit)
    1   Unknown Species (Stéphane Richard)	(PD)	(Wootha/Edit)
    2   Worker 05 (Stéphane Richard)	(PD)	(Wootha/Edit)
    3   Red 02 (Stéphane Richard)	(PD)	(Wootha/Edit)
    4   Worker 01 (Stéphane Richard)	(PD)	(Wootha/Edit)
    5   Wrecked Ships (Stéphane Richard)	(PD)	(Wootha/Edit)
    6   Worker 04 (Stéphane Richard)	(PD)	(Wootha/Edit)
    7   Stop Smiling (Stéphane Richard)	(PD)	(Wootha/Edit)
    8   Hunter Hunting (Stéphane Richard)	(PD)	(Wootha/Edit)
    9   Stop Smiling (Stéphane Richard)	(PD)	(Wootha/Edit)
    10  Grind or Glide (Stéphane Richard)	(PD)	(Wootha/Edit)
    11  Mother of Dragons (Stéphane Richard)	(PD)	(Wootha/Edit)
    12  Grind or Glide (Stéphane Richard)	(PD)	(Wootha/Edit)
    13  What Lies Ahead (Stéphane Richard)	(PD)	(Wootha/Edit)
    14  Painting Final 8K (Stéphane Richard)	(PD)	(Wootha/Edit)
    15  Down the Rabbit Hole (Stéphane Richard)	(PD)	(Wootha/Edit)

# BANNER
	xmas2020 (Jesusalva)	(CC BY)	(Contains artwork from David Revoy)

