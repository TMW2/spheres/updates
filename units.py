#!/usr/bin/python3

import os, subprocess, cv2

# This is a much more complex script. BY ALL MEANS use base folder

pwd = os.getcwd()
f = open("ATTR.txt", "w")
bf1=[]
subprocess.call("cp ready-units/base/* ready-units/", shell=True)
FILES = list(os.listdir(pwd + "/ready-units"))

for it in FILES:
    if not it.endswith("png") and not it.endswith("jpg"):
        continue
    if it.startswith("sq_"):
        continue

    name = it.split(".")[0]
    path = pwd + "/ready-units/"
    sqne = "sq_%s" % it

    try:
        unitid = int(name)
    except:
        print("ERROR: %s is not parseable" % it)
        continue

    print("%s" % it)

    """
    # Force to PNG
    if it.endswith("jpg"):
        # WARNING: DESTRUCTIVE
        subprocess.call("convert %s/%s %s/%s.png" % (path, it, path, it), shell=True)
        subprocess.call("rm %s/%s" % (path, it), shell=True)
        it = "%s.png" % it
    """

    # Resizing
    img = cv2.imread(path+it)
    w, h = img.shape[:2]
    if h != 640 or w != 960:
        # Resize the image, relying entirely in ImageMagick
        # Preserving aspect ratio is turned OFF (Crop would be better)
        # WARNING: DESTRUCTIVE
        subprocess.call("convert %s/%s -resize 640x960\! %s/%s" % (path, it, path, it), shell=True)
        print("[OK] Image resized")

    # A square doesn't exists
    # Generate one with opencv
    if not sqne in FILES:
        cPath="lbpcascade_animeface.xml"
        iPath="ready-units/%s" % it
        cascade=cv2.CascadeClassifier(cPath)
        org_img=cv2.imread(iPath)
        img=cv2.cvtColor(org_img, cv2.COLOR_BGR2GRAY)
        maxi = 3.2
        step = 0.1
        curr = 1.0
        faces = []
        # Detect faces
        while len(faces) != 1:
            if curr == 1.0:
                faces = cascade.detectMultiScale(img, scaleFactor=1.01, minNeighbors=5)
            else:
                faces = cascade.detectMultiScale(img, scaleFactor=curr, minNeighbors=5)
            curr+=step
            if curr > maxi:
                break

        if len(faces) != 1:
            print("\033[1mERROR: %s has %d faces, no square possible!\033[0m" % (it, len(faces)))
            bf1.append("* ERROR: %s needs a square\n" % it)
            continue

        face=faces[0]

        # Calculate offset
        xoff=max(0, 340-face[2])/2
        yoff=max(0, 340-face[3])/2

        x=max(0, face[0]-xoff)
        y=max(0, face[1]-yoff)

        subprocess.call("convert %s -crop %dx%d+%d+%d %s/%s" % (iPath, 340, 340, x, y, path, sqne), shell=True)
        print("[OK] Success finding face at (%d,%d)" % (x, y))

    # Resize the squared image (DESTRUCTIVE)
    # FIXME: Add alpha channel
    subprocess.call("convert %s/%s -resize 340x340\! -alpha on %s/%s.png" % (path, sqne, path, sqne), shell=True)

    # Format the squared image (DESTRUCTIVE)
    subprocess.call("convert %s/%s.png mask.png -alpha off -compose CopyOpacity -composite %s/%s.webp" % (path, sqne, path, sqne), shell=True)
    sqne+=".webp"

    # Make copies
    i=0
    while i < 3:
        subprocess.call("convert %s/%s %s/unit/10%04d%02d.webp" % (path, it, path, unitid, i), shell=True)
        subprocess.call("convert %s/%s %s/square/10%04d%02d.webp" % (path, sqne, path, unitid, i), shell=True)
        i+=1

    # Save to attribution
    bf1.append("\t10%04d NAME (AUTHOR)\t(CC BY SA)\t(SOURCE)\n" % (unitid))

# Save attribution file
for it in sorted(bf1):
    f.write(it)
f.close()

# Remove the working files
# IF YOU DID NOT USE BASE FOLDER, CONSIDER THE FILES FORSAKEN
subprocess.call("rm ready-units/*.*", shell=True)

