#!/usr/bin/python2.7
########################################################################################
#	 This file is part of Spheres.
#	 Copyright (C) 2019  Jesusalva

#	 This library is free software; you can redistribute it and/or
#	 modify it under the terms of the GNU Lesser General Public
#	 License as published by the Free Software Foundation; either
#	 version 2.1 of the License, or (at your option) any later version.

#	 This library is distributed in the hope that it will be useful,
#	 but WITHOUT ANY WARRANTY; without even the implied warranty of
#	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#	 Lesser General Public License for more details.

#	 You should have received a copy of the GNU Lesser General Public
#	 License along with this library; if not, write to the Free Software
#	 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# This builds live/ folder for Update Server
import subprocess, os, uuid, traceback, hashlib, copy
PATH="/var/www/html/assets"

def md5sum(f):
    md5=hashlib.md5()
    fp=open(f, "rb")
    ct=fp.read()
    md5.update(ct)
    rt=copy.copy(md5.hexdigest())
    fp.close()
    del ct
    return rt

# Delete previous deployment
subprocess.call("rm -rf %s/*" % PATH, shell=True)


# Go through every file in raw and give them a hash
for path, subdir, files in os.walk("raw/"):
    for n in files:
        fn=os.path.join(path, n)

        # Ignored files
        if fn.endswith("~"):
            continue

        # Copy and update resource table
        print(fn)
        try:
            NAME=fn.replace("raw/", "").replace("/", "_")
            EXT=NAME.split(".")[-1:][0]
            NAME=NAME.split(".")[:-1][0]
            ## TODO code
            if EXT in ["png", "jpg", "jpeg"]:
                subprocess.call("convert %s %s/%s.webp" % (fn, PATH, NAME), shell=True)
                subprocess.call("mv %s/%s.webp %s/%s" % (PATH, NAME, PATH, NAME), shell=True)
                EXT="webp"
            elif EXT in ["xcf", "psd"]:
                continue
            elif EXT in ["mp3", "ogg", "webp"]:
                subprocess.call("cp %s %s/%s" % (fn, PATH, NAME), shell=True)
            else:
                subprocess.call("cp %s %s/%s.%s" % (fn, PATH, NAME, EXT), shell=True)
        except:
            traceback.print_exc()
            pass

# Create blank index page to disable dir listing
subprocess.call("touch %s/index.html" % PATH, shell=True)

# Zip everything and provide an all-in-one download
fn="%s/%s.zip" % (PATH, uuid.uuid4().hex)
subprocess.call('cd %s; find . -printf "%%P\n" | zip -X -9 -@ %s' % (PATH, fn), shell=True)
mdi = md5sum(fn)
subprocess.call("echo \"%s#%s\" > %s/fetch.txt" % (fn.replace(PATH, ""), mdi, PATH), shell=True)

# Correct permissions
#subprocess.call("chown -R www-data:www-data %s/*" % PATH, shell=True)
subprocess.call("chmod --recursive 444 %s/*" % PATH, shell=True)

print("FILENAME: %s" % fn)
print("Register it in \"token\" value of server config to serve all-in-one downloads.")
print("(This feature might not yet have been implemented, though)")

# FIXME: Maybe give the ZIP file link on the index.html page?
